version: 2

models:
  - name: wk_self_managed_spo
    description: Workspace model that builds a MVC for Self-Managed SPO and SPU KPIs.
    columns:
      - name: organization_id
        description: A unique id that is a combination of the host_name and dim_instance_id.
        tests:
          - not_null
      - name: delivery
        description: delivery is based on the product category name and is either SaaS or Self-Managed delivery.
        tests:
          - not_null
      - name: organization_type
        description: segmentation for organization type. Values - Group, Individual, User
        tests:
          - not_null
          - accepted_values:
              values: ['Group','Individual']
      - name: active_stage_count
        description: number of active stages for a given organization in a given month


  - name: wk_saas_spo
    description: Workspace model that shows monthly Stages per Organization data on a namespace-level for SaaS.
    columns:
      - name: reporting_month
        description: Month in which the product usage was reported.
        tests:
          - not_null
      - name: organization_id
        description: Top-level namespace id.
        tests:
          - not_null
      - name: delivery
        description: delivery is based on the product category name and is either SaaS or Self-Managed delivery.
        tests:
          - not_null
      - name: organization_type
        description: segmentation for organization type. Values - Group, Individual, User
        tests:
          - not_null
          - accepted_values:
              values: ['Group','Individual','User']
      - name: product_tier
        description: segmentation for Product Tier. Values - Core (CE, EE), Starter, Premium, Ultimate, NULL
        tests:
          - not_null
      - name: is_paid_product_tier
        description: Denotes whether the organization is on a paid product tier at the time of reporting.
        tests:
          - not_null
      - name: active_stage_count
        description: number of active stages for a given organization in a given month

  - name: wk_usage_ping_geo_node_usage
    description: Extract and flatten data from the geo nodes
    columns:
      - name: instance_path_id
        description: the unique ID for the combination of the ping_id and the path (unique service ping identifier and the metrics path).
        tests:
          - not_null
          - unique

  - name: fct_ping_instance_metric
    description: ' {{ doc("fct_ping_instance_metric") }} '
    columns:
      - name: ping_instance_metric_id
        tests:
          - not_null
        description: the unique composite ID for the service_ping_instance_metric model consisting of dim_ping_instance_id and flattened_high_level.metrics_path
      - name: dim_ping_instance_id
        description: The unqiue id of a service ping insatnce in the dim_ping_instance model.
      - name: dim_product_tier_id
        description: The unique id of a Product Tier in the dim_product_tier model.
      - name: dim_location_country_id
      - name: dim_ping_date_id
        description: The unique id of a country from the dim_location_country model
      - name: dim_host_id
        description: The unique id of a host from the dim_host model (brought in from source data).
      - name: dim_instance_id
        description: The unique uuid of an instance from the dim_instances model.
      - name: dim_date_id
        description: The unqiue id of a date in the dim_date model.
      - name: dim_license_id
        description:  The unique id of a license from the dim_license model.
      - name: metrics_path
        description: The unique JSON key path of identifier of the metric in the ping payload. This appears as key_path in the metric definition YAML files
      - name: metric_value
        description: The value associated with the metric path
      - name: dim_installation_id
        description: The unique identifier of the installation, easily joined to dim_installation. This id is the combination of dim_host_id and dim_instance_id and is considered the unique identifier of the instance for reporting and analysis
      - name: ping_created_at
        description: The time when the ping was created
      - name: ping_created_at_date
        description: The date when the ping was created
      - name: data_source
        description: The source application where the data was extracted from.
      - name: has_timed_out
        description: A flag to denote that it has timed out
      - name: dim_subscripton_id
        description: The unique id of a subscription from the dim_subscription model
      - name: dim_subscripton_license_id
        description: The unique id of a license subscription
      - name: time_frame
        description: The time frame associated with the metric, as defined in the metric definition YAML file
      - name: umau_value
        description: The unique monthly active user value for the instance

  - name: fct_ping_instance_metric_28_day
    description: ' {{ doc("fct_ping_instance_metric_28_day") }} '
    columns:
      - name: ping_instance_metric_id
        tests:
          - not_null
        description: the unique composite ID for the service_ping_instance_metric model consisting of dim_ping_instance_id and flattened_high_level.metrics_path
      - name: dim_ping_instance_id
        description: The unqiue id of a service ping insatnce in the dim_ping_instance model.
      - name: dim_product_tier_id
        description: The unique id of a Product Tier in the dim_product_tier model.
      - name: dim_location_country_id
      - name: dim_ping_date_id
        description: The unique id of a country from the dim_location_country model
      - name: dim_host_id
        description: The unique id of a host from the dim_host model (brought in from source data).
      - name: dim_instance_id
        description: The unique uuid of an instance from the dim_instances model.
      - name: dim_date_id
        description: The unqiue id of a date in the dim_date model.
      - name: dim_license_id
        description:  The unique id of a license from the dim_license model.
      - name: metrics_path
        description: The unique JSON key path of identifier of the metric in the ping payload. This appears as key_path in the metric definition YAML files
      - name: metric_value
        description: The value associated with the metric path
      - name: dim_installation_id
        description: The unique identifier of the installation, easily joined to dim_installation. This id is the combination of dim_host_id and dim_instance_id and is considered the unique identifier of the instance for reporting and analysis
      - name: ping_created_at
        description: The time when the ping was created
      - name: ping_created_at_date
        description: The date when the ping was created
      - name: data_source
        description: The source application where the data was extracted from.
      - name: has_timed_out
        description: A flag to denote that it has timed out
      - name: dim_subscripton_id
        description: The unique id of a subscription from the dim_subscription model
      - name: dim_subscripton_license_id
        description: The unique id of a license subscription
      - name: time_frame
        description: The time frame associated with the metric, as defined in the metric definition YAML file
      - name: umau_value
        description: The unique monthly active user value for the instance

  - name: fct_ping_instance_metric_7_day
    description: ' {{ doc("fct_ping_instance_metric_7_day") }} '
    columns:
      - name: ping_instance_metric_id
        tests:
          - not_null
        description: the unique composite ID for the service_ping_instance_metric model consisting of dim_ping_instance_id and flattened_high_level.metrics_path
      - name: dim_ping_instance_id
        description: The unqiue id of a service ping insatnce in the dim_ping_instance model.
      - name: dim_product_tier_id
        description: The unique id of a Product Tier in the dim_product_tier model.
      - name: dim_location_country_id
      - name: dim_ping_date_id
        description: The unique id of a country from the dim_location_country model
      - name: dim_host_id
        description: The unique id of a host from the dim_host model (brought in from source data).
      - name: dim_instance_id
        description: The unique uuid of an instance from the dim_instances model.
      - name: dim_date_id
        description: The unqiue id of a date in the dim_date model.
      - name: dim_license_id
        description:  The unique id of a license from the dim_license model.
      - name: metrics_path
        description: The unique JSON key path of identifier of the metric in the ping payload. This appears as key_path in the metric definition YAML files
      - name: metric_value
        description: The value associated with the metric path
      - name: dim_installation_id
        description: The unique identifier of the installation, easily joined to dim_installation. This id is the combination of dim_host_id and dim_instance_id and is considered the unique identifier of the instance for reporting and analysis
      - name: ping_created_at
        description: The time when the ping was created
      - name: ping_created_at_date
        description: The date when the ping was created
      - name: data_source
        description: The source application where the data was extracted from.
      - name: has_timed_out
        description: A flag to denote that it has timed out
      - name: dim_subscripton_id
        description: The unique id of a subscription from the dim_subscription model
      - name: dim_subscripton_license_id
        description: The unique id of a license subscription
      - name: time_frame
        description: The time frame associated with the metric, as defined in the metric definition YAML file
      - name: umau_value
        description: The unique monthly active user value for the instance

  - name: fct_ping_instance_metric_all_time
    description: ' {{ doc("fct_ping_instance_metric_all_time") }} '
    columns:
      - name: ping_instance_metric_id
        tests:
          - not_null
        description: the unique composite ID for the service_ping_instance_metric model consisting of dim_ping_instance_id and flattened_high_level.metrics_path
      - name: dim_ping_instance_id
        description: The unqiue id of a service ping insatnce in the dim_ping_instance model.
      - name: dim_product_tier_id
        description: The unique id of a Product Tier in the dim_product_tier model.
      - name: dim_location_country_id
      - name: dim_ping_date_id
        description: The unique id of a country from the dim_location_country model
      - name: dim_host_id
        description: The unique id of a host from the dim_host model (brought in from source data).
      - name: dim_instance_id
        description: The unique uuid of an instance from the dim_instances model.
      - name: dim_date_id
        description: The unqiue id of a date in the dim_date model.
      - name: dim_license_id
        description:  The unique id of a license from the dim_license model.
      - name: metrics_path
        description: The unique JSON key path of identifier of the metric in the ping payload. This appears as key_path in the metric definition YAML files
      - name: metric_value
        description: The value associated with the metric path
      - name: dim_installation_id
        description: The unique identifier of the installation, easily joined to dim_installation. This id is the combination of dim_host_id and dim_instance_id and is considered the unique identifier of the instance for reporting and analysis
      - name: ping_created_at
        description: The time when the ping was created
      - name: ping_created_at_date
        description: The date when the ping was created
      - name: data_source
        description: The source application where the data was extracted from.
      - name: has_timed_out
        description: A flag to denote that it has timed out
      - name: dim_subscripton_id
        description: The unique id of a subscription from the dim_subscription model
      - name: dim_subscripton_license_id
        description: The unique id of a license subscription
      - name: time_frame
        description: The time frame associated with the metric, as defined in the metric definition YAML file
      - name: umau_value
        description: The unique monthly active user value for the instance



  - name: mart_ping_instance_metric
    description: ' {{ doc("mart_ping_instance_metric") }} '
    columns:
      - name: ping_instance_metric_id
        tests:
          - not_null
          #- unique
        description: the unique composite ID for the mart_ping_instance_metric model consisting of dim_ping_instance_id and metrics_path
      - name: dim_date_id
        description: The unqiue id of a date in the dim_date model.
      - name: metrics_path
        description: The unique JSON key path of identifier of the metric in the ping payload. This appears as key_path in the metric definition YAML files
      - name: metric_value
        description: The value associated with the metric path
      - name: dim_ping_instance_id
        description: The unique identifier of the ping
      - name: dim_instance_id
        description: The unique identifier of the instance, easily joined to dim_installation. This id is store in the database of the instance and appears as uuid in the ping payload
      - name: dim_license_id
        description: The unique identifier of the license, easily joined to dim_license
      - name: dim_installation_id
        description: The unique identifier of the installation, easily joined to dim_installation. This id is the combination of dim_host_id and dim_instance_id and is considered the unique identifier of the instance for reporting and analysis
      - name: latest_active_subscription_id
        description:  The unique id of the latest active subscription
      - name: dim_billing_account_id
        description: The identifier of the Zuora account associated with the subscription
      - name: dim_parent_crm_account_id
        description: The identifier of the ultimate parent account, easily joined to dim_crm_account
      - name: dim_host_id
        description: The unique identifier of the host, easily joined to dim_installation or dim_host
      - name: major_minor_version_id
        description: The id of the major minor version, defined as major_version*100 + minor_version. This id is intended to facilitate easy filtering on versions
      - name: host_name
        description: The name (URL) of the host
      - name: service_ping_delivery_type
        description: How the product is delivered to the instance (Self-Managed, SaaS)
      - name: ping_edition
        description: The edition of GitLab on the instance (EE, CE)
      - name: ping_product_tier
        description: The product tier of the ping, inferred from the edition and the plan saved in the license (Core, Starter Premium, Ultimate)
      - name: ping_edition_product_tier
        description: The concatenation of ping_edition and ping_product_tier (EE - Core - Free, EE - Premium, etc.)
      - name: major_version
        description: The major version of GitLab on the instance. For example, for 13.6.2, major_version is 13. See details here (https://docs.gitlab.com/ee/policy/maintenance.html)
      - name: minor_version
        description: The major version of GitLab on the instance. For example, for 13.6.2, minor_version is 6. See details here (https://docs.gitlab.com/ee/policy/maintenance.html)
      - name: major_minor_version
        description: The concatenation of major and minor version. For example, for 13.6.2, major_minor_version is 13.6
      - name: version_is_prerelease
        description: Boolean flag which is set to True if the version is a pre-release Version of the GitLab App. See more details here (https://docs.gitlab.com/ee/policy/maintenance.html)
      - name: is_internal
        description: Denotes whether the instance is internal
      - name: is_staging
        description: Boolean flag which is set to True if the instance has been identified as a staging instance
      - name: is_trial
        description: Boolean flag which is set to True if the instance has a valid trial license at service ping creation
      - name: umau_value
        description: The unique monthly active user value for the instance
      - name: stage_name
        description: The name of the product stage responsible for the metric
      - name: section_name
        description: The name of the product section responsible for the metric
      - name: group_name
        description: The name of the product group responsible for the metric
      - name: is_smau
        description: Boolean flag set to True if the metrics is one of the counters chosen for the stage's SMAU calculation
      - name: is_gmau
        description: Boolean flag set to True if the metrics is one of the counters chosen for the group's GMAU calculation
      - name: is_paid_gmau
        description: Boolean flag set to True if the metrics is one of the counters chosen for the group's paid GMAU calculation
      - name: is_umau
        description: Denotes whether the event is identified as the stage's UMAU (unique monthly active user) metric. See [Page](https://internal-handbook.gitlab.io/product/performance-indicators/) for metric definitions
      - name: time_frame
        description: The time frame associated with the metric, as defined in the metric definition YAML file
      - name: instance_user_count
        description: The total count of users on the instance
      - name: subscription_name_slugify
        description: If a subscription is linked to the license, slugified name of the subscription
      - name: subscription_start_month
        description: The month when the subscription linked to the license started
      - name: subscription_end_month
        description: The month when the subscription linked to the license is supposed to end according to last agreed terms
      - name: product_category_array
        description: An array containing all of the product tier names associated associated with the subscription
      - name: product_rate_plan_name_array
        description: An array containing all of the product rate plan names associated with the subscription
      - name: is_paid_subscription
        description: Boolean flag set to True if the subscription has a ARR > 0
      - name: is_program_subscription
        description: Boolean flag set to True if the subscription is under an EDU or OSS Program
      - name: crm_account_name
        description: The name of the crm account coming from SFDC
      - name: parent_crm_account_name
        description: The name of the ultimate parent account coming from SFDC
      - name: parent_crm_account_billing_country
        description: The billing country of the ultimate parent account coming from SFDC
      - name: parent_crm_account_sales_segment
        description: The sales segment of the ultimate parent account from SFDC. Sales Segments are explained here (https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#segmentation)
      - name: parent_crm_account_industry
        description: The industry of the ultimate parent account from SFDC
      - name: parent_crm_account_owner_team
        description: The owner team of the ultimate parent account from SFDC
      - name: parent_crm_account_sales_territory
        description: The sales territory of the ultimate parent account from SFDC
      - name: technical_account_manager
        description: The name of the technical account manager of the subscription
      - name: ping_created_at
        description: The time when the ping was created
      - name: ping_created_at_month
        description: The month when the ping was created
      - name: is_last_ping_of_month
        description: Boolean flag set to True if this is the instance's (defined by dim_installation_id) last ping of the calendar month (defined by ping_created_at)

  - name: mart_ping_instance_metric_28_day
    description: ' {{ doc("mart_ping_instance_metric_28_day") }} '
    columns:
      - name: ping_instance_metric_id
        tests:
          - not_null
          - unique
        description: the unique composite ID for the mart_ping_instance_metric model consisting of dim_ping_instance_id and metrics_path
      - name: dim_date_id
        description: The unqiue id of a date in the dim_date model.
      - name: metrics_path
        description: The unique JSON key path of identifier of the metric in the ping payload. This appears as key_path in the metric definition YAML files
      - name: metric_value
        description: The value associated with the metric path
      - name: dim_ping_instance_id
        description: The unique identifier of the ping
      - name: dim_instance_id
        description: The unique identifier of the instance, easily joined to dim_installation. This id is store in the database of the instance and appears as uuid in the ping payload
      - name: dim_license_id
        description: The unique identifier of the license, easily joined to dim_license
      - name: dim_installation_id
        description: The unique identifier of the installation, easily joined to dim_installation. This id is the combination of dim_host_id and dim_instance_id and is considered the unique identifier of the instance for reporting and analysis
      - name: latest_active_subscription_id
        description:  The latest child dim_subscription_id of the subscription linked to the license
      - name: dim_billing_account_id
        description: The identifier of the Zuora account associated with the subscription
      - name: dim_parent_crm_account_id
        description: The identifier of the ultimate parent account, easily joined to dim_crm_account
      - name: dim_host_id
        description: The unique identifier of the host, easily joined to dim_installation or dim_host
      - name: major_minor_version_id
        description: The id of the major minor version, defined as major_version*100 + minor_version. This id is intended to facilitate easy filtering on versions
      - name: host_name
        description: The name (URL) of the host
      - name: service_ping_delivery_type
        description: How the product is delivered to the instance (Self-Managed, SaaS)
      - name: ping_edition
        description: The edition of GitLab on the instance (EE, CE)
      - name: ping_product_tier
        description: The product tier of the ping, inferred from the edition and the plan saved in the license (Core, Starter Premium, Ultimate)
      - name: ping_edition_product_tier
        description: The concatenation of ping_edition and ping_product_tier (EE - Core - Free, EE - Premium, etc.)
      - name: major_version
        description: The major version of GitLab on the instance. For example, for 13.6.2, major_version is 13. See details here (https://docs.gitlab.com/ee/policy/maintenance.html)
      - name: minor_version
        description: The major version of GitLab on the instance. For example, for 13.6.2, minor_version is 6. See details here (https://docs.gitlab.com/ee/policy/maintenance.html)
      - name: major_minor_version
        description: The concatenation of major and minor version. For example, for 13.6.2, major_minor_version is 13.6
      - name: version_is_prerelease
        description: Boolean flag which is set to True if the version is a pre-release Version of the GitLab App. See more details here (https://docs.gitlab.com/ee/policy/maintenance.html)
      - name: is_internal
        description: Denotes whether the instance is internal
      - name: is_staging
        description: Boolean flag which is set to True if the instance has been identified as a staging instance
      - name: is_trial
        description: Boolean flag which is set to True if the instance has a valid trial license at service ping creation
      - name: umau_value
        description: The unique monthly active user value for the instance
      - name: stage_name
        description: The name of the product stage responsible for the metric
      - name: section_name
        description: The name of the product section responsible for the metric
      - name: group_name
        description: The name of the product group responsible for the metric
      - name: is_smau
        description: Boolean flag set to True if the metrics is one of the counters chosen for the stage's SMAU calculation
      - name: is_gmau
        description: Boolean flag set to True if the metrics is one of the counters chosen for the group's GMAU calculation
      - name: is_paid_gmau
        description: Boolean flag set to True if the metrics is one of the counters chosen for the group's paid GMAU calculation
      - name: is_umau
        description: Boolean flag set to True if the metrics is one of the counters chosen for UMAU calculation
      - name: time_frame
        description: The time frame associated with the metric, as defined in the metric definition YAML file
      - name: instance_user_count
        description: The total count of users on the instance
      - name: subscription_name_slugify
        description: If a subscription is linked to the license, slugified name of the subscription
      - name: subscription_start_month
        description: The month when the subscription linked to the license started
      - name: subscription_end_month
        description: The month when the subscription linked to the license is supposed to end according to last agreed terms
      - name: product_category_array
        description: An array containing all of the product tier names associated associated with the subscription
      - name: product_rate_plan_name_array
        description: An array containing all of the product rate plan names associated with the subscription
      - name: is_paid_subscription
        description: Boolean flag set to True if the subscription has a ARR > 0
      - name: is_program_subscription
        description: Boolean flag set to True if the subscription is under an EDU or OSS Program
      - name: crm_account_name
        description: The name of the crm account coming from SFDC
      - name: parent_crm_account_name
        description: The name of the ultimate parent account coming from SFDC
      - name: parent_crm_account_billing_country
        description: The billing country of the ultimate parent account coming from SFDC
      - name: parent_crm_account_sales_segment
        description: The sales segment of the ultimate parent account from SFDC. Sales Segments are explained here (https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#segmentation)
      - name: parent_crm_account_industry
        description: The industry of the ultimate parent account from SFDC
      - name: parent_crm_account_owner_team
        description: The owner team of the ultimate parent account from SFDC
      - name: parent_crm_account_sales_territory
        description: The sales territory of the ultimate parent account from SFDC
      - name: technical_account_manager
        description: The name of the technical account manager of the subscription
      - name: ping_created_at
        description: The time when the ping was created
      - name: ping_created_at_month
        description: The month when the ping was created
      - name: is_last_ping_of_month
        description: Boolean flag set to True if this is the instance's (defined by dim_installation_id) last ping of the calendar month (defined by ping_created_at)

  - name: mart_ping_instance_metric_7_day
    description: ' {{ doc("mart_ping_instance_metric_7_day") }} '
    columns:
      - name: ping_instance_metric_id
        tests:
          - not_null
          - unique

  - name: mart_ping_instance_metric_all_time
    description: ' {{ doc("mart_ping_instance_metric_all_time") }} '
    columns:
      - name: ping_instance_metric_id
        tests:
          - not_null
          - unique

  - name: rpt_ping_instance_metric_adoption_monthly_all
    description: ' {{ doc("rpt_ping_instance_metric_adoption_monthly_all") }} '
    columns:
      - name: rpt_ping_instance_metric_adoption_monthly_all_id
        tests:
          - not_null
          - unique

  - name: rpt_ping_instance_metric_adoption_subscription_monthly
    description: ' {{ doc("rpt_ping_instance_metric_adoption_subscription_monthly") }} '
    columns:
      - name: rpt_ping_instance_metric_adoption_subscription_monthly_id
        tests:
          - not_null
          - unique

  - name: rpt_ping_instance_metric_adoption_subscription_metric_monthly
    description: ' {{ doc("rpt_ping_instance_metric_adoption_subscription_metric_monthly") }} '
    columns:
      - name: rpt_ping_instance_metric_adoption_subscription_metric_monthly_id
        tests:
          - not_null
          - unique
        description: the unique composite ID for the rpt_ping_instance_metric_adoption_monthly model consisting of reporting_month, metrics_path, and estimation_grain
      - name: reporting_month
        description: The month the dat was reported
      - name: reporting_count
        description: The number of seats/subscriptions that reported the metric
      - name: no_reporting_count
        description: The number of seats/subscriptions that didn't report the metric, but should have
      - name: estimation_grain
        description: The grain the estimation is being applied to (i.e. subscription based estimation, seat based estimation)
      - name: percent_reporting
        description: The percentage of events being reported
      - name: stage_name
        description: The name of the product stage responsible for the metric
      - name: section_name
        description: The name of the product section responsible for the metric
      - name: group_name
        description: The name of the product group responsible for the metric
      - name: is_smau
        description: Boolean flag set to True if the metrics is one of the counters chosen for the stage's SMAU calculation
      - name: is_gmau
        description: Boolean flag set to True if the metrics is one of the counters chosen for the group's GMAU calculation
      - name: is_paid_gmau
        description: Boolean flag set to True if the metrics is one of the counters chosen for the group's paid GMAU calculation
      - name: is_umau
        description: Boolean flag set to True if the metrics is one of the counters chosen for UMAU calculation


  - name: rpt_ping_instance_metric_estimated_monthly
    description: ' {{ doc("rpt_ping_instance_metric_estimated_monthly") }} '
    columns:
      - name: rpt_ping_instance_metric_estimated_monthly_id
        tests:
          - not_null
          - unique
        description: the unique composite ID for the rpt_ping_instance_metric_estimated_monthly model consisting of reporting_month, metrics_path, estimation_grain, ping_edition_product_tier, and service_ping_delivery_type
      - name: metrics_path
        description: The unique JSON key path of identifier of the metric in the ping payload. This appears as key_path in the metric definition YAML files
      - name: reporting_month
        description: The month the data was reported
      - name: ping_edition
        description: The edition of GitLab on the instance (EE, CE)
      - name: actual_usage
        description: The number of events that were reported
      - name: estimated_usage
        description: The estimated number of events that happened. but were not actually reported
      - name: reporting_count
        description: The number of seats/subscriptions that reported the metric
      - name: no_reporting_count
        description: The number of seats/subscriptions that didn't report the metric, but should have
      - name: estimation_grain
        description: The grain the estimation is being applied to (i.e. subscription based estimation, seat based estimation)
      - name: percent_reporting
        description: The percentage of events being reported
      - name: total_estimated_usage
        description: The total estimated number of events that happened. but were not actually reported
      - name: ping_product_tier
        description: The product tier of the ping, inferred from the edition and the plan saved in the license (Core, Starter Premium, Ultimate)
      - name: ping_main_edition_product_tier
        description: A combination of the main_edition and the ping_product_tier (EE - Self-Managed - Free, EE - Self-Managed - Premium, EE - Self-Managed - Ultimate)
      - name: stage_name
        description: The name of the product stage responsible for the metric
      - name: section_name
        description: The name of the product section responsible for the metric
      - name: group_name
        description: The name of the product group responsible for the metric
      - name: is_smau
        description: Boolean flag set to True if the metrics is one of the counters chosen for the stage's SMAU calculation
      - name: is_gmau
        description: Boolean flag set to True if the metrics is one of the counters chosen for the group's GMAU calculation
      - name: is_paid_gmau
        description: Boolean flag set to True if the metrics is one of the counters chosen for the group's paid GMAU calculation
      - name: is_umau
        description: Boolean flag set to True if the metrics is one of the counters chosen for UMAU calculation

  - name: rpt_ping_counter_statistics
    description: ' {{ doc("rpt_ping_counter_statistics") }} '
    columns:
      - name: rpt_ping_counter_statistics_id
        tests:
          - not_null
          - unique

  - name: rpt_ping_instance_subcription_opt_in_monthly
    description: ' {{ doc("rpt_ping_instance_subcription_opt_in_monthly") }} '
    columns:
      - name: rpt_ping_instance_subcription_opt_in_monthly_id
        tests:
          - not_null
          - unique

  - name: rpt_ping_instance_subcription_metric_opt_in_monthly
    description: ' {{ doc("rpt_ping_instance_subcription_metric_opt_in_monthly") }} '
    columns:
      - name: rpt_ping_instance_subcription_metric_opt_in_monthly_id
        tests:
          - not_null
          - unique

  - name: fct_performance_indicator_targets
    description: '{{ doc("fct_performance_indicator_targets") }}'
    columns:
      - name: fct_performance_indicator_targets_id
        description: the unique ID of a month/metric record in fct_performance_indicator_targets
        tests:
